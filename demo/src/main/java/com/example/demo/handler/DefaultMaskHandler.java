package com.example.demo.handler;

import com.diboot.core.data.protect.DefaultDataMaskHandler;
import org.springframework.stereotype.Component;

/**
 * 默认的数据脱敏显示处理
 * @author JerryMa
 * @version v3.0.0
 * @date 2022/9/9
 * Copyright © diboot.com
 */
//@Component
public class DefaultMaskHandler extends DefaultDataMaskHandler {

}